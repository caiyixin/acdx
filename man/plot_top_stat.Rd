\name{plot_top_stat}
\alias{plot_top_stat}
\title{Volcano-like plot}
\description{Gene scatter plot of significance versus effect size}
\usage{
plot_top_stat( ttab, n_names=40, alpha=0.05, onesided=TRUE, 
  cex=1,pch=20,lwd=2,
  ... )
}
\arguments{
  \item{ttab}{Ouput of \code{\link{top}}}
  \item{n_names}{Number of gene names shown}
  \item{alpha}{Significance level}
  \item{onesided}{Show one-sided significance (ignore the negative effects)}
  \item{cex}{Point size}
  \item{pch}{Point shape}
  \item{lwd}{Line width}
  \item{...}{Other options to \code{\link{plot.default}}}
}
