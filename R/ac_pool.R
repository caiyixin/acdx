ac_pool <- function( ac0, poolmap )
{
  ac <- list()
  class(ac) <- "ac"
  
  ac$N <- array(0,dim=c(nrow(ac0$N),length(poolmap)))
  rownames(ac$N) <- rownames(ac0$N)
  colnames(ac$N) <- names(poolmap)
  for(i in names(poolmap))
    for(j in poolmap[[i]])
      ac$N[,i] <- ac$N[,i] + ac0$N[,j]

  ac$y <- array(0,dim=c(2,nrow(ac$N),dim(ac0$y)[3],ncol(ac$N)))
  dimnames(ac$y) <- list(dimnames(ac0$y)[[1]],rownames(ac$N),
    dimnames(ac0$y)[[3]],colnames(ac$N))
  for(i in names(poolmap))
    {
    message(i, ":")
    for(j in poolmap[[i]])
      {
      message("\t",j)
      ac$y[1,,,i] <- ac$y[1,,,i] + ifelse(!is.na(ac0$y[1,,,j]),
        ac0$y[1,,,j] * ac0$N[,j],0)
      ac$y[2,,,i] <- ac$y[2,,,i] + ac0$N[,j] * ac0$y[2,,,j]
      }
    #ac$y[1,,,i] <- ac$y[1,,,i] / ac$N[,i]
    ac$y[2,,,i] <- ac$y[2,,,i] / ac$N[,i]
    }

  ac$aggr_scale <- sum_scale(ac$y)

  ac
}
