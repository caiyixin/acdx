ac_pbulk <- function ( ac, cg = NULL, normalize=TRUE )
{
  if(is.null(cg)) cg <- 1:ncol(ac$N)
  yb <- apply(array(
    apply( ac$y[1,,,], 2, function(u) ifelse(ac$N > 0, ac$N*u,0)),
    dim=c(dim(ac$y)[c(2,4,3)]),
    dimnames=dimnames(ac$y)[c(2,4,3)]
    ),c(1,3),sum)
  if(normalize)
    {
    sz <- rowSums(yb)
    yb <- yb / sz * mean(sz)
    }
  t(yb)
}
